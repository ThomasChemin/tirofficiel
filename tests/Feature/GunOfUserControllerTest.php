<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GunOfUserControllerTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }

    /**
     * Test if a user logged can access to gun/add.
     * @return void
     */
    public function testAddGun() {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/gun/add/Pistol');
        $response->assertStatus(200);
    }

    /**
     * Test if a user logged can access to gun/show
     * @return void
     */
    public function testShowGun() {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/gun/show');
        $response->assertStatus(200);
    }

}
