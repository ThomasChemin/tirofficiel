<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GunControllerTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
        $this->user = \App\Models\User::find(1);
        $this->userTwo = \App\Models\User::find(2);
    }

    /**
     * Test if a user logged with admin roles can access to gun/add.
     * @return void
     */
    public function testAdminAddGun() {
        $response = $this->actingAs($this->user)->get('/adminGun/add');
        $response->assertStatus(200);
    }

    /**
     * Test if a user logged with admin roles can access to gun/add.
     * @return void
     */
    public function testAdminDeleteGun() {
        $response = $this->actingAs($this->user)->get('/adminGun/delete');
        $response->assertStatus(200);
        $response = $this->actingAs($this->user)->get('/adminGun/delete/Pistol');
        $response->assertStatus(200);
  
        $response = $this->actingAs($this->userTwo)->get('/adminGun/delete');
        $response->assertStatus(302);
        $response = $this->actingAs($this->userTwo)->get('/adminGun/delete/Pistol');
        $response->assertStatus(302);
    }

}
