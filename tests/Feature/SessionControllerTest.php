<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SessionControllerTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }

    /**
     * Test if a user logged can access to showSession.
     *
     * @return void
     */
    public function testShowSession() {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/session/show');
        $response->assertStatus(200);
    }

    /**
     * Test if a user logged can access to addSession
     * @return void
     */
    public function testAddSession() {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/session/add');
        $response->assertStatus(200);
    }

    /**
     * Test if a user logged can access to updateSession
     * @return void
     */
    public function testUpdateSession() {
        $user = \App\Models\User::find(1);
        $session = \App\Models\Session::where('idUser', '=', $user->id)->first();
        $response = $this->actingAs($user)->get('/session/update/' . $session->id);
        $response->assertStatus(200);
    }

    /**
     * Test if a user logged can access to deleteSession
     * @return void
     */
    public function testDeleteSession(){
        $user = \App\Models\User::find(1);
        $session = \App\Models\Session::where('idUser', '=', $user->id)->first();
        $response = $this->actingAs($user)->get('/session/delete/' . $session->id);
        $response->assertStatus(200);
    }
    
}
