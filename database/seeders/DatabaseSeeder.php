<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $user = \App\Models\User::create([
                    'name' => 'admin',
                    'email' => "admin@gmail.com",
                    'email_verified_at' => now(),
                    'password' => \Illuminate\Support\Facades\Hash::make("administrator"),
                    'remember_token' => \Illuminate\Support\Str::random(10)
        ]);
        \App\Models\Right::factory(1)->create();
        \App\Models\User::factory(1)->create();
        \App\Models\UserRight::factory(1)->create();
        \App\Models\Gun::factory(1)->create();
        \App\Models\Session::factory(1)->create();
        \App\Models\UserRight::create([
            'idUser' => $user->id,
            'idRight' => '1',
        ]);
        \App\Models\UserRight::create([
            'idUser' => $user->id,
            'idRight' => '2',
        ]);
    }

}
