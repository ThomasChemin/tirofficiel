<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session', function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->date("dateSession");
            $table->string("distance");
            $table->integer("nbShoot");
            $table->boolean("cleaned");
            $table->string("caliber");
            $table->integer("idGun");
            $table->integer("idUser");
            $table->foreign('idGun')->references('id')->on('guns')->onDelete('cascade');
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session');
    }
}
