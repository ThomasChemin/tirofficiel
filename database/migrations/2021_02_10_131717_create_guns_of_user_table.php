<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGunsOfUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gunsOfUser', function (Blueprint $table) {
            $table->integer("idUser");
            $table->integer("idGun");
            $table->primary(['idUser', 'idGun']);
            $table->string('placeOfPurchase');
            $table->date('dateOfPurchase');
            $table->date('dateOfSale')->nullable();
            $table->string('soldToUser')->nullable();
            $table->foreign('idGun')->references('id')->on('guns')->onDelete('cascade');
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gunsOfUser');
    }
}
