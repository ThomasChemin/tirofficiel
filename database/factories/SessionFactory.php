<?php

namespace Database\Factories;

use App\Models\Session;
use Illuminate\Database\Eloquent\Factories\Factory;

class SessionFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Session::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'dateSession' => '2021-01-01',
            'caliber' => '22',
            'idGun' => '1',
            'distance' => '50',
            'nbShoot' => '50',
            'cleaned' => "1",
            'idUser' => 1,
        ];
    }

}
