<?php

namespace Database\Factories;

use App\Models\Gun;
use Illuminate\Database\Eloquent\Factories\Factory;

class GunFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gun::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'CZ 452',
            'category' => 'Pistol'
        ];
    }
}
