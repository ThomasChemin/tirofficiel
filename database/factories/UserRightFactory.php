<?php

namespace Database\Factories;

use App\Models\UserRight;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserRightFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserRight::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'idUser' => 2,
            'idRight' => 2,
        ];
    }

}
