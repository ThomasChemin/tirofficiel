<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\GunController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GunOfUserController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');

/**
 * Routes for sessions
 */
Route::group(['prefix' => 'session'], function() {
    Route::get('/add', [SessionController::class, 'showAddSession'])->middleware('auth')->name('addSession');
    Route::post('/doAdd', [SessionController::class, 'doAddSession'])->middleware('auth')->name('doAddSession');
    Route::get('/update/{idSession}', [SessionController::class, 'showUpdateSession'])->middleware('auth')->name('updateSession');
    Route::post('/doUpdate', [SessionController::class, 'doUpdateSession'])->middleware('auth')->name('doUpdateSession');
    Route::get('/delete/{idSession}', [SessionController::class, 'showDeleteSession'])->middleware('auth')->name('deleteSession');
    Route::post('/doDelete', [SessionController::class, 'doDeleteSession'])->middleware('auth')->name('doDeleteSession');
    Route::get('/show/{annee?}', [SessionController::class, 'showSession'])->middleware('auth')->name('showSession');
});

/**
 * Routes for guns
 */
Route::group(['prefix' => 'gun'], function() {
    Route::get('/add/{category}', [GunOfUserController::class, 'showAddGunByCategory'])->middleware('auth')->name('addGun');
    Route::post('/doAdd', [GunOfUserController::class, 'doAddGun'])->middleware('auth')->name('doAddGun');
    Route::get('/show', [GunOfUserController::class, 'showGunsOfUser'])->middleware('auth')->name('showGunsOfUser');
    Route::post('/sell', [GunOfUserController::class, 'sellGun'])->middleware('auth')->name('sellGun');
    Route::post('/doSell', [GunOfUserController::class, 'doSellGun'])->middleware('auth')->name('doSellGun');
    Route::post('/detail', [GunOfUserController::class, 'detailGun'])->middleware('auth')->name('detailGun');
});

/**
 * Routes for guns only for admin
 */
Route::group(['prefix' => 'adminGun'], function() {
    Route::get('/add', [GunController::class, 'adminAddGun'])->middleware('Admin')->name('adminAddGun');
    Route::post('/doAdd', [GunController::class, 'adminDoAddGun'])->middleware('Admin')->name('adminDoAddGun');
    Route::get('/delete/{category?}', [GunController::class, 'adminDeleteGunByCategory'])->middleware('Admin')->name('adminDeleteGun');
    Route::post('/doDelete', [GunController::class, 'adminDoDeleteGun'])->middleware('Admin')->name('adminDoDeleteGun');
});
