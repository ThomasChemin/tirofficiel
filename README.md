## Sommaire
1. [Tir Officiel](#tir-officiel)
2. [Status](#status)
3. [Visiteur](#visiteur)
4. [Admin](#admin)
5. [Mise en place du projet](#mise-place-projet)
6. [Connexion en tant qu'administrateur](#connexion-administrateur)

## Tir Officiel
<a name="tir-officiel"></a>
Le projet Tir Officiel permet a des utilisateurs de s'enregistrer, de se connecter pour renseigner des sessions de tir qu'ils ont pu faire.
Il permet de référencer toutes les informations nécessaires aux personnes pratiquant le tir sportif.
***
## Status
<a name="status"></a>
Terminé
***
## Visiteur
<a name="visiteur"></a>
Un visiteur peut s'enregistrer et se connecter via les formulaires fournit par Laravel.
Il peut ajouter une arme à sa collection en fonction de celles disponibles dans la base de données. Il peut supprimer une arme de sa collection.
Il peut (s'il possède une arme) renseigner une session où il indique la date, l'arme, le nombre de tir, la distance de tir, s'il a nettoyé l'arme, ainsi que le calibre utilisé.
Il peut par la suite voir toutes les sessions par année (de octobre à septembre). Et supprimer une session.
***
## Admin
<a name="admin"></a>
Un admin a accès au back-end et peut alors ajouter une arme à la base de données, supprimer une arme de la base de données.
Une arme supprimée de la base de données supprime toutes les sessions où l'arme est présente et, la supprime pour les utilisateurs qui la possèdent.
***
## Mise en place du projet
<a name="mise-place-projet"></a>
Une fois le projet cloné, créer un .env en suivant l'example du .env.example. Remplacer les champs avec vos informations.
Une fois le .env créer faite la commande suivante dans la racine de votre projet : 

> ./artisan key:generate

Pour générer la base de données, faites les commandes suivantes dans la racine de votre projet. Pour générer les tables : 

>./artisan migrate:fresh 

Placez vous dans le répertoire SQL, ouvrez un terminal puis tapez la commande suivante : 

> mysql -u root -p votreBDD < weapons.sql.

Enfin, retournez à la racine de votre projet puis tapez la commande suivante pour générer des données :

>./artisan db:seed 
***
## Connexion en tant qu'administrateur
<a name="connexion-administrateur"></a>
Login : admin@gmail.com

Mot de passe : administrator
***
