@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Supprimer une arme') }}</div>

                <div class="card-body" style="margin-bottom: 150px">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successDelete'))
                    <script>
                        var success = (function succes() {
                            alert("{{old('successDelete')}}");
                        }());
                    </script>
                    @endif
                    <div class="container">
                        <h5>Choisir une catégorie</h5>
                        <p>Catégorie actuelle : {{$guns[0]->category}}</p>
                        @foreach($categories as $category)
                        @if(!$category->category == null)
                        <a class="btn btn-primary" style="margin-right: 10px; margin-bottom: 20px;width: 110px" href="{{route('adminDeleteGun', ['category' => $category->category])}}">{{$category->category}}</a>
                        @endif
                        @endforeach
                    </div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <th>Nom</th>
                        <th>Supprimer</th>
                        </thead>
                        <tbody>
                            @foreach($guns as $gun)
                            <tr>
                                <td>{{$gun->name}}</td>
                                <td>
                                    <form action="{{route('adminDoDeleteGun')}}" method='POST'>
                                        @csrf
                                        <input type="hidden" value="{{$gun->id}}" name="idGun">
                                        <input type="hidden" value="{{$gun->category}}" name="category">
                                        <input class="btn btn-danger" type="submit" onclick="return confirm('Voulez-vous vraiment supprimer l\'arme ?')" value="Supprimer">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination" style='display: flex;
                         justify-content: center;'>
                        {{ $guns->onEachSide(0)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><a id="aToTopPage" class="btn btn-primary" href="#topPage">Haut de page</a></div>
</div>
@endsection
