@extends('layouts.app')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ajouter une arme') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successInsert'))
                    <script>
var success = (function succes() {
    alert("{{old('successInsert')}}");
}());
                    </script>
                    @endif
                    <form action="{{route('adminDoAddGun')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Nom de l'arme : </label>
                            <input class='form-control float-right' type="text" name="gunName" value="{{old('gunName')}}">
                            <label class="col-sm-12 col-form-label">Catégorie de l'arme : </label>
                            <select class='custom-select float-right' name="gunCategory">
                                @foreach($categories as $category)
                                @if($category->category != null)
                                <option value="{{$category->category}}">{{$category->category}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="text-center">
                            <input class="btn btn-primary" type="submit" value="Ajouter">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
