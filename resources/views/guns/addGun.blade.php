@extends('layouts.app')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-bottom: 100px">
                <div class="card-header">{{ __('Ajouter une arme') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successInsert'))
                    <script>
var success = (function succes() {
    alert("{{old('successInsert')}}");
}());
                    </script>
                    @endif
                    <div class="container">
                        <h5>Choisir une catégorie</h5>
                        <p>Catégorie actuelle : {{$guns[0]->category}}</p>
                        <div class="col-lg-12 col-md-12 col-ls-12 col-xs-12">
                            @foreach($categories as $category)
                            @if(!$category->category == null)
                            <a class="btn btn-primary" style="margin-right: 10px; margin-bottom: 20px;width: 110px"  href="{{route('addGun', ['category' => $category->category])}}">{{$category->category}}</a>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <br>
                    <form action="{{route('doAddGun')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label"> Arme : </label>
                            @if(count($guns) > 0)
                            <select id="weapon" class='custom-select float-right' name="weapon">
                                @foreach($guns as $gun)
                                <option value="{{$gun->id}}" {{old('weapon') == $gun->id ? 'selected="selected"' : '' }}>
                                    {{$gun->name}}
                                </option>
                                @endforeach
                            </select>
                            <label class="col-sm-12 col-form-label"> Date d'achat : </label>
                            <input class='form-control float-right' type="date" name="dateOfPurchase">
                            <label class="col-sm-12 col-form-label"> Lieu d'achat : </label>
                            <input class='form-control float-right' type="text" name="placeOfPurchase">
                        </div>
                        <div>
                            <div class="text-center">
                                <input class="btn btn-primary" type="submit" name="Envoyer" value="Ajouter">
                            </div>
                            @else
                            <p class="col-md-8 offset-2 text-center">
                                Il n'y a pas d'armes pour la catégorie recherchée.
                            </p>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#weapon').select2();
    });
</script>
@endsection
