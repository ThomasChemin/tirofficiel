@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vendre une arme') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="text-center">
                        <form action="{{route('doSellGun')}}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <input type="hidden" value="{{$gunOfUser->gun->id}}" name="idGun">
                                Date de vente
                                <input class='form-control float-right' type="date" name="dateOfSale" value="{{old('dateOfSale')}}">
                                Acheteur
                                <input class='form-control float-right' type="text" name="soldToUser" placeholder="Nom Prénom" value="{{old('soldToUser')}}">
                            </div>
                            <input class="btn btn-danger" type="submit" value="Vendre">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
