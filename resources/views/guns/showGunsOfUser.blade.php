@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        @foreach($errors->all() as $error)
        <p style="color: red">{{ $error }}</p>
        @endforeach
        @if(old('successUpdate'))
        <script>
            var success = (function success() {
                alert("{{old('successUpdate')}}");
            }());
        </script>
        @endif
        @if(count($gunsOfUser) != 0)
        <div class="col" style="margin-bottom: 150px;">
            <table class="table table-striped" data-toggle="table" data-search="true" data-show-columns="true">
                <thead class="thead-dark">
                <th data-sortable="true" data-field="name">Nom</th>
                <th data-sortable="true" data-field="category">Catégorie</th>
                <th>Détails</th>
                <th data-sortable="true">Vendre</th>
                </thead>
                <tbody>
                    @foreach($gunsOfUser as $gunOfUser)
                    <tr>
                        <td>{{$gunOfUser->gun->name}}</td>
                        <td>{{$gunOfUser->gun->category}}</td>
                        <td>
                            <form action="{{route('detailGun')}}" method='POST'>
                                @csrf
                                <input type="hidden" value="{{$gunOfUser->idGun}}" name="idGun">
                                <input class="btn btn-primary" type="submit" value="Detail" name="detail">
                            </form>
                        </td>
                        @if($gunOfUser->dateOfSale == null)
                        <td>
                            <form action="{{route('sellGun')}}" method='POST'>
                                @csrf
                                <input type="hidden" value="{{$gunOfUser->idGun}}" name="idGun">
                                <input class="btn btn-danger" type="submit" value="Vendre" name="sell">
                            </form>
                        </td>
                        @else
                        <td>
                            Vendu
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pagination" style='display: flex;
                 justify-content: center;'>
                {{ $gunsOfUser->links() }}
            </div>
        </div>
        @else
        Vous n'avez pas enregistré d'arme.
        @endif
    </div>
</div>
@endsection
