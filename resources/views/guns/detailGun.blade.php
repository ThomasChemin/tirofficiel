@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Détail de l\'arme : ') . $gunOfUser->gun->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="text-center">
                        <table class="table table-striped">
                            <tr>
                                <th class="thead-dark">
                                    Nom
                                </th>
                                <td>
                                    {{$gunOfUser->gun->name}}
                                </td>
                            </tr>
                            <tr>
                                <th class="thead-dark">
                                    Catégorie
                                </th>
                                <td>
                                    {{$gunOfUser->gun->category}}
                                </td>
                            </tr>
                            <tr>
                                <th class="thead-dark">
                                    Nombre de tir
                                </th>
                                <td>
                                    {{$nbShoot}}
                                </td>
                            </tr>
                            <tr>
                                <th class="thead-dark">
                                    Lieu d'achat
                                </th>
                                <td>
                                    {{$gunOfUser->placeOfPurchase}}
                                </td>
                            </tr>
                            <tr>
                                <th class="thead-dark">
                                    Date d'achat
                                </th>
                                <td>
                                    {{$gunOfUser->dateOfPurchase}}
                                </td>
                            </tr>
                            @if($gunOfUser->soldToUser != null)
                            <tr>
                                <th class="thead-dark">
                                    Vendu à 
                                </th>
                                <td>
                                    {{$gunOfUser->soldToUser}}
                                </td>
                            </tr>
                            <tr>
                                <th class="thead-dark">
                                    Date de vente 
                                </th>
                                <td>
                                    {{$gunOfUser->dateOfSale}}
                                </td>
                            </tr>
                            @else
                            <tr>
                                <th class="thead-dark" colspan="2">
                                    Non vendu
                                </th>
                            </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
