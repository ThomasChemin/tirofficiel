@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Modification d\'une session') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successUpdate'))
                    <script>
                        var success = (function succes() {
                            alert("{{old('successUpdate')}}");
                        }());
                    </script>
                    @endif
                    <form action="{{route('doUpdateSession')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="idSession" value="{{$session->id}}">
                            <label class="col-sm-12 col-form-label"> Date de la session : </label>
                            <input class='form-control float-right' type="date" name="dateSession" value="{{$session->dateSession}}">
                            <label class="col-sm-12 col-form-label"> Arme utilisée : </label>
                            <select class='custom-select float-right' name="weaponUsed">
                                @foreach($gunsOfUser as $gunOfUser)
                                <option value="{{$gunOfUser->gun->id}}" @if($gunOfUser->gun->id == $session->gun->id) selected @endif>
                                    {{$gunOfUser->gun->name}}
                                </option>
                                @endforeach
                            </select>

                            <label class="col-sm-12 col-form-label"> Distance : </label>
                            <input class='form-control float-right' type="text" name="distance" placeholder="Distance de la cible (en mètre)" value="{{$session->distance}}">
                            <label class="col-sm-12 col-form-label"> Nombre de tir : </label>
                            <input class='form-control float-right' type="text" name="nbTir" placeholder="Nom de balle utilisées" value="{{$session->nbShoot}}">
                            <label class="col-sm-12 col-form-label"> Calibre : </label>
                            <input class='form-control float-right' type="text" name="caliber" placeholder="Calibre utilisé" value="{{$session->caliber}}">
                            <label class="col-sm-12 col-form-label"> Nettoyage : </label>
                            <select class='custom-select float-right' name="cleaned">
                                <option value="0">
                                    Non
                                </option>
                                <option value="1" @if($session->cleaned == 1) selected @endif>
                                    Oui
                                </option>
                            </select>
                        </div>
                        <div class="text-center">
                            <input class="float-left btn btn-danger" type="reset" value="Réinitialiser">
                            <input class="float-right btn btn-primary" type="submit" name="Envoyer" value="Envoyer">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
