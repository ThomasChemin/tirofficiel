@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-bottom: 100px">
                <div class="card-header">{{ __('Ajouter une session') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successInsert'))
                    <script>
                        var success = (function succes() {
                            alert("{{old('successInsert')}}");
                        }());
                    </script>
                    @endif
                    @if(count($gunsOfUser) > 0)
                    <form action="{{route('doAddSession')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label"> Date de la session : </label>
                            <input class='form-control float-right' type="date" name="dateSession" value="{{old("dateSession")}}">
                            <label class="col-sm-12 col-form-label"> Arme utilisée : </label>

                            <select class='custom-select float-right' name="weaponUsed">
                                @foreach($gunsOfUser as $gunOfUser)
                                <option value="{{$gunOfUser->gun->id}}" {{old('weaponUsed') == $gunOfUser->gun->id ? 'selected="selected"' : '' }}>
                                    {{$gunOfUser->gun->name}}
                                </option>
                                @endforeach
                            </select>

                            <label class="col-sm-12 col-form-label"> Distance : </label>
                            <input class='form-control float-right' type="text" name="distance" placeholder="Distance de la cible (en mètre)" value="{{old('distance')}}">
                            <label class="col-sm-12 col-form-label"> Nombre de tir : </label>
                            <input class='form-control float-right' type="text" name="nbTir" placeholder="Nom de balle utilisées" value="{{old('nbTir')}}">
                            <label class="col-sm-12 col-form-label"> Calibre : </label>
                            <input class='form-control float-right' type="text" name="caliber" placeholder="Calibre utilisé" value="{{old('caliber')}}">
                            <label class="col-sm-12 col-form-label"> Nettoyage : </label>
                            <select class='custom-select float-right' name="cleaned">
                                <option value="0">
                                    Non
                                </option>
                                <option value="1" {{old('cleaned') == 1 ? 'selected="selected"' : '' }}>
                                    Oui
                                </option>
                            </select>
                        </div>
                        <div class="text-center">
                            <input class="btn btn-primary" type="submit" name="Envoyer" value="Ajouter">
                        </div>
                    </form>
                    @else
                    Vous n'avez pas d'arme et par conséquent, vous ne pouvez pas enregistrer de session.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
