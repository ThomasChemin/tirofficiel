@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10"><h2>Mes sessions</h2></div>
        <div class="col-12">
            <div class="dropdown float-right">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Années
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @for($i = $firstYear->format('Y'); $i <= $lastYear->format('Y'); $i++)
                    <a class="dropdown-item" href="{{route('showSession', ['annee' => $i])}}">{{$i}}</a>
                    @endfor
                </div>
            </div>
            <p>Nombre de session depuis l'année {{$firstYear->format('Y')}} : {{$nbTotalSession}}</p>
            <p>Nombre de session cette année : {{$nbSessionThisYear}}</p>
            <p>Année : {{$annee}}</p>
        </div>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        @if(old('successDelete'))
        <script>
            var success = (function succes() {
                alert("{{old('successDelete')}}");
            }());
        </script>
        @endif
        @if(count($sessions) != 0)
        <div class="container">
            <div class="col-12">
                <table class="table-sm table-striped" data-toggle="table" data-search="true" data-show-columns="true">
                    <caption>Résumé des sessions cette année</caption>
                    <thead class="thead-dark">
                        <tr>
                            <th data-sortable="true">Date</th>
                            <th data-sortable="true">Arme</th>
                            <th data-sortable="true">Calibre</th>
                            <th data-sortable="true">Distance</th>
                            <th data-sortable="true">Nombre de tir</th>
                            <th data-sortable="true">Nettoyage</th>
                            <th data-sortable="true">Modifier/Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sessions as $session)
                        <tr>
                            {{-- Si c'est la première date alors, on l'affiche. Si c'est la date d'avant est la même que celle actuelle alors, on le l'affiche pas'--}}
                            <td>@if($loop->index >= 1) @if($sessions[$loop->index-1]->dateSession != $session->dateSession) {{$session->dateSession}} @endif @else {{$session->dateSession}} @endif</td>
                            <td>{{$session->gun->name}}</td>
                            <td>@if($session->caliber == null) Non référencé @else {{$session->caliber}}@endif</td>
                            <td>{{$session->distance}}</td>
                            <td>{{$session->nbShoot}}</td>
                            <td>@if($session->cleaned == 1) <i class='fas fa-check' style='font-size:24px; color: #04ff04'></i> @else <i class="fas fa-times" style='font-size:24px; color: red'></i> @endif</td>
                            <td>
                                <a href="{{route('updateSession', ['idSession' => $session->id])}}" class="btn btn-primary">Modifier</a>
                                <a href="{{route('deleteSession', ['idSession' => $session->id])}}" class="btn btn-danger">Supprimer</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination" style='display: flex;
                     justify-content: center;'>
                    {{ $sessions->links() }}
                </div>
            </div>
        </div>
        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
            <table class="table-sm table-striped" data-toggle="table" data-search="true" data-pagination="true" data-show-columns="true">
                <caption>Nombre de tir par arme</caption>
                <thead class="thead-dark">
                    <tr>
                        <th data-sortable="true">Arme</th>
                        <th data-sortable="true">Catégorie</th>
                        <th data-sortable="true">Cette année</th>
                        <th data-sortable="true">Depuis inscription</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-success">
                        <th scope="row" colspan=2>Total</th>
                        <td>{{$gunsOfUser->sum('nbShoot')}}</td>
                        <td>{{$gunsOfUser->sum('nbShootTotal')}}</td>
                    </tr>
                    @foreach($gunsOfUser as $gun)
                    <tr @if($gun->nbShoot == 0) class="table-danger" @endif>
                        <td>{{$gun->gun->name}}</td>
                        <td>{{$gun->gun->category}}</td>
                        <td>{{$gun->nbShoot}}</td>
                        <td>{{$gun->nbShootTotal}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
            <table class="table-sm table-striped" data-toggle="table" data-search="true">
                <caption>Nombre de session par mois</caption>
                <thead class="thead-dark">
                    <tr class="table-success">
                        <th data-sortable="true">Mois</th>
                        <th data-sortable="true">Cette année</th>
                        <th data-sortable="true">Depuis inscription</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-success">
                        <th>Total</th>
                        <td>{{$nbSessionThisYear}}</td>
                        <td>{{$nbTotalSession}}</td>
                    </tr>
                    @foreach($nbSessionPerMonth['thisSession'] as $month => $nb)
                    <tr>
                        <td>{{$month}}</td>
                        <td>{{$nb}}</td>
                        <td>{{$nbSessionPerMonth['allSession'][$month]}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="container">
            <div class="col-12" style="margin-top: 50px; margin-bottom: 150px">
                <table class="table-sm table-striped" data-toggle="table" data-search="true" data-show-columns="true">
                    <caption>Nombre de tir par calibre</caption>
                    <thead class="thead-dark">
                        <tr>
                            <th data-sortable="true">Calibre</th>
                            <th data-sortable="true">Cette année</th>
                            <th data-sortable="true">Depuis inscription</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-success">
                            <th>Total</th>
                            <td>{{$shotPerCaliber['totalThisYear']}}</td>
                            <td>{{$shotPerCaliber['totalBeginning']}}</td>
                        </tr>
                        @foreach($shotPerCaliber['thisYear'] as $caliber => $nb)
                        <tr>
                            <td>{{$caliber}}</td>
                            <td>{{$nb}}</td>
                            <td>{{$shotPerCaliber['fromBeginning'][$caliber]}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
        <p class="text-center">Vous n'avez pas de session pour l'année {{$annee}}</p>
        @endif
    </div>
    <div><a id="aToTopPage" class="btn btn-primary" href="#topPage">Haut de page</a></div>
</div>

@endsection
