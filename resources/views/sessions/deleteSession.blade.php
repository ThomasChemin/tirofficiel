@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Supprimer une session') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    <div>
                        <h4>Informations sur la session</h4>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>Date de la session</th>
                                    <td>{{$session->dateSession}}</td>
                                </tr>
                                <tr>
                                    <th>Arme utilisée</th>
                                    <td>{{$session->gun->name}}</td>
                                </tr>
                                <tr>
                                    <th>Distance</th>
                                    <td>{{$session->distance}}</td>
                                </tr>
                                <tr>
                                    <th>Nombre de tir</th>
                                    <td>{{$session->nbShoot}}</td>
                                </tr>
                                <tr>
                                    <th>Calibre</th>
                                    <td>{{$session->caliber}}</td>
                                </tr>
                                <tr>
                                    <th>Nettoyage</th>
                                    <td>@if($session->cleaned==1)<i class='fas fa-check' style='font-size:24px; color: #04ff04'></i> @else <i class="fas fa-times" style='font-size:24px; color: red'> @endif</td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <form method="POST" action="{{route('doDeleteSession')}}">
                                @csrf
                                <input type="hidden" name="idSession" value="{{$session->id}}">
                                <input class="form-control float-right" type="text" name="supprimer" placeholder="Ecrivez : Supprimer">
                                <div>
                                    <input class="btn btn-danger float-right" type="submit" name="delete" value="Supprimer">
                                    <a class="btn btn-primary float-left" href="{{route('showSession')}}">Annuler</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
