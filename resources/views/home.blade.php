@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <div class="text-center">
            @if(!Auth::check())
            <p class="h4">
                Bienvenue sur TirOfficiel.<br>
                Vous pouvez enregistrer vos sessions de tir avec vos armes.<br>
                Pour cela vous devez vous connecter. <br>
                <a href="{{route('login')}}" class="btn btn-primary">Se connecter</a><br>
                Si vous n'avez pas de compte, inscrivez-vous !<br>
                <a href="{{route('register')}}" class="btn btn-primary">S'inscrire</a>
            </p>
            @else
            <p class="h4">
                Bienvenue sur TirOfficiel {{Auth::user()->name}}
            </p>
            @endif
        </div>
    </div>
</div>
@endsection
