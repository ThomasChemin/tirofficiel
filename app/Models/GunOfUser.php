<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GunOfUser extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'gunsOfUser';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idUser', 'idGun'];

    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the gun.
     */
    public function gun() {
        return $this->hasOne(Gun::class, 'id', 'idGun');
    }

    /**
     * Get the user that own the gun.
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'idUser');
    }

}
