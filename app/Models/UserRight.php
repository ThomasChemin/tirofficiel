<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRight extends Model
{
    use HasFactory;
    
    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'userRight';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idUser', 'idRight'];

    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the gun.
     */
    public function right() {
        return $this->hasOne(Right::class, 'id', 'idRight');
    }

    /**
     * Get the user.
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'idUser');
    }
}
