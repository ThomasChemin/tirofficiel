<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
    use HasFactory;
    
    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'right';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;
}
