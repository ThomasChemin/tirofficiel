<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (Auth::check()) {
            $rights = Auth::user()->rights;
            foreach ($rights as $right) {
                if ($right->idRight == 1) {
                    return $next($request);
                }
            }
        }
        return redirect()->back();
    }

}
