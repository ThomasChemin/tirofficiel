<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;
use App\Models\Gun;
use App\Models\GunOfUser;
use Illuminate\Http\Request;

class GunController extends Controller {

    /**
     * Display the page adminAddGun
     * @return view adminAddGun
     */
    public function adminAddGun() {
        $categories = Gun::select('category')
                ->distinct('category')
                ->get();
        return view('admin.adminAddGun', ['categories' => $categories]);
    }

    /**
     * Add a new gun in the database if it doesn't exist
     * @param Request $request
     * @return back with input and errors if it fails | to a new adminAddGun with success message if it success
     */
    public function adminDoAddGun(Request $request) {
        $validData = Validator::make($request->all(), [
                    'gunName' => ['required', 'unique:guns,name'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        try {
            Gun::create([
                'name' => $request->get('gunName'),
                'category' => $request->get('gunCategory'),
            ]);
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorInsert' => 'Une erreur est survenue lors de l\'ajout de l\'arme.']);
        }
        return redirect()->route('adminAddGun')->withInput(['successInsert' => 'L\'arme a bien été ajoutée.']);
    }
    
    /**
     * Display the page adminDeleteGun by category
     * @param type $category
     * @return view adminDeleteGun | page 404
     */
    public function adminDeleteGunByCategory($category = 'Pistol'){
        $guns = Gun::where('category', '=', $category)
                ->orderBy('name')
                ->paginate(15);
        $categories = Gun::select('category')
                ->distinct('category')
                ->get();
        if(count($guns) == 0){
            return view('errors.404');
        }
        return view('admin.adminDeleteGun', ['guns' => $guns, 'categories' => $categories]);
    }
    
    /**
     * Delete a gun from the database
     * @param Request $request
     * @return back with input and errors if it fails | to a new route adminDeleteGun with success message if it success
     */
    public function adminDoDeleteGun(Request $request){
        try {
            $gun = Gun::where('id', '=', $request->get('idGun'))->delete();
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorDelete' => 'Une erreur est survenue lors de la suppression de l\'arme.']);
        }
        return redirect()->route('adminDeleteGun', ['category' => $request->get('category')])->withInput(['successDelete' => 'La suppression de l\'arme a bien eu lieu.']);
    }
    
    /**
     * Get the error messages for the defined validation rules.
     * @return type
     */
    public function messages() {
        return [
            'gunName.unique' => 'L\'arme existe déjà',
            'required' => 'Le champ :attribute doit être rempli.',
        ];
    }

}
