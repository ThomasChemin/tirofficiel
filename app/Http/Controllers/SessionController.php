<?php

namespace App\Http\Controllers;

use \App\Models\GunOfUser;
use \App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Validator;

class SessionController extends Controller {

    /**
     * Display the page showAddSession
     * @return type view addSession
     */
    public function showAddSession() {
        $gunsOfUser = GunOfUser::where(['idUser' => Auth::id()])
                ->where('dateOfSale', '=', null)
                ->get();
        return view('sessions.addSession', ["gunsOfUser" => $gunsOfUser]);
    }

    /**
     * Add a Session in the database for the current user
     * @param Request $request the information of the form
     * @return type return back with input if an error occur | return to a new addSession if success
     */
    public function doAddSession(Request $request) {
        $validData = Validator::make($request->all(), [
                    'dateSession' => ['required'],
                    'distance' => ['required', 'regex:/[0-9]+/'],
                    'nbTir' => ['required', 'regex:/[0-9]+/'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $session = new Session([
            "dateSession" => $request->get('dateSession'),
            "distance" => $request->get('distance'),
            "nbShoot" => $request->get('nbTir'),
            "cleaned" => $request->get('cleaned'),
            "caliber" => $request->get('caliber'),
            "idGun" => $request->get('weaponUsed'),
            "idUser" => Auth::id(),
        ]);
        try {
            $session->save();
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorInsert' => 'Une erreur est survenue. Veillez réessayer']);
        }
        return redirect()->route('addSession')->withInput(['successInsert' => 'Votre session a bien été enregistrées.']);
    }

    /**
     * Display the page showSession with the parameters needed :
     * number of session for the current year ($nbSessionThisYear), 
     * number of session from the beginning ($nbTotalSession),
     * number of session per month ($nbSessionPerMonth),
     * number of bullet shoot per weapon this year, number of bullet shoot per weapon from the beginning
     * number of bullet shoot per caliber this year and from beginning ($shotPerCaliber)
     * @param type $annee the year of the session
     * @return type view showSession with parameters
     */
    public function showSession($annee = null) {
        if ($annee == null) {
            $annee = new \DateTime();
        } else {
            $annee = new \DateTime($annee . "-09-30");
        }
        if ($annee->format('m') >= 10) {
            $from = new \DateTime();
            $from->setDate($annee->format('Y'), 10, 1);
        } else {
            $from = new \DateTime();
            $from->setDate($annee->format('Y') - 1, 10, 1);
        }
        $to = $annee;

        $sessions = Session::where(["idUser" => Auth::id()])
                ->whereBetween("dateSession", [$from, $to])
                ->orderBy("dateSession")
                ->paginate(10);
        if (count($sessions) == 0) {
            return redirect()->back();
        }

        $allSessionByUser = Session::select("dateSession")
                ->where(["idUser" => Auth::id()])
                ->orderBy("dateSession")
                ->distinct("dateSession")
                ->get();
        $nbSessionThisYear = DB::table("session")->select("dateSession")->where("idUser", "=", Auth::id())->whereBetween('dateSession', [$from, $to])->distinct("dateSession")->count();
        $nbTotalSession = $allSessionByUser->count("dateSession");

        $firstYear = $allSessionByUser[0]->dateSession;
        $firstYear = new \DateTime($firstYear);

        $lastYear = $allSessionByUser[count($allSessionByUser) - 1]->dateSession;
        $lastYear = new \DateTime($lastYear);

        $gunsOfUser = Session::select('idGun')
                ->where(["idUser" => Auth::id()])
                ->orderBy("idGun")
                ->groupBy('idGun')
                ->distinct()
                ->get();
        foreach ($gunsOfUser as $gun) {
            $tempGunTotalShootFromFirstYear = Session::select('nbShoot')
                    ->where(["idUser" => Auth::id()])
                    ->where("dateSession", '>=', $firstYear)
                    ->where("idGun", '=', $gun->gun->id)
                    ->get();
            $tempGunTotalShootThisYear = Session::select('nbShoot')
                    ->where(["idUser" => Auth::id()])
                    ->whereBetween("dateSession", [$from, $to])
                    ->where("idGun", '=', $gun->gun->id)
                    ->get();
            $gun->nbShoot = $tempGunTotalShootThisYear->sum('nbShoot');
            $gun->nbShootTotal = $tempGunTotalShootFromFirstYear->sum('nbShoot');
        }

        $shotPerCaliber = [
            'thisYear' => [],
            'fromBeginning' => [],
            'totalThisYear' => 0,
            'totalBeginning' => 0,
        ];
        $sessionCaliberShoot = Session::select('caliber')
                ->where(["idUser" => Auth::id()])
                ->orderBy("caliber")
                ->distinct("caliber")
                ->get();
        foreach ($sessionCaliberShoot as $session) {
            $tempCaliberTotalShootFromBeginning = Session::select('nbShoot')
                    ->where("idUser", '=', Auth::id())
                    ->where("caliber", '=', $session->caliber)
                    ->get();
            $shotPerCaliber['fromBeginning'][$session->caliber] = $tempCaliberTotalShootFromBeginning->sum('nbShoot');
            $shotPerCaliber['totalBeginning'] += $tempCaliberTotalShootFromBeginning->sum('nbShoot');
            $tempGunTotalShootThisYear = Session::select('nbShoot')
                    ->where("idUser", '=', Auth::id())
                    ->whereBetween("dateSession", [$from, $to])
                    ->where("caliber", '=', $session->caliber)
                    ->get();
            $shotPerCaliber['thisYear'][$session->caliber] = $tempGunTotalShootThisYear->sum('nbShoot');
            $shotPerCaliber['totalThisYear'] += $tempGunTotalShootThisYear->sum('nbShoot');
        }
        $nbSessionPerMonth = ["thisSession" => [
                "Oct" => 0,
                "Nov" => 0,
                "Dec" => 0,
                "Jan" => 0,
                "Feb" => 0,
                "Mar" => 0,
                "Apr" => 0,
                "May" => 0,
                "Jun" => 0,
                "Jul" => 0,
                "Aug" => 0,
                "Sep" => 0,
            ], "allSession" => [
                "Oct" => 0,
                "Nov" => 0,
                "Dec" => 0,
                "Jan" => 0,
                "Feb" => 0,
                "Mar" => 0,
                "Apr" => 0,
                "May" => 0,
                "Jun" => 0,
                "Jul" => 0,
                "Aug" => 0,
                "Sep" => 0,
        ]];
        $sessionThisYear = Session::select("dateSession")->where(["idUser" => Auth::id()])
                ->whereBetween("dateSession", [$from, $to])
                ->orderBy("dateSession")
                ->distinct("dateSession")
                ->get();
        foreach ($sessionThisYear as $session) {
            $dateSession = new \DateTime($session->dateSession);
            foreach ($nbSessionPerMonth['thisSession'] as $month => $nb) {
                if ($dateSession->format('M') == $month) {
                    $nbSessionPerMonth['thisSession'][$month] += 1;
                }
            }
        }
        foreach ($allSessionByUser as $session) {
            $dateSession = new \DateTime($session->dateSession);
            foreach ($nbSessionPerMonth['allSession'] as $month => $nb) {
                if ($dateSession->format('M') == $month) {
                    $nbSessionPerMonth['allSession'][$month] += 1;
                }
            }
        }


        return view("sessions.showSession",
                ["sessions" => $sessions,
                    "lastYear" => $lastYear,
                    "nbSessionPerMonth" => $nbSessionPerMonth,
                    "firstYear" => $firstYear,
                    "nbTotalSession" => $nbTotalSession,
                    "annee" => ($annee->format('Y') . " - " . ($annee->format('Y') + 1)),
                    "gunsOfUser" => $gunsOfUser,
                    "nbSessionThisYear" => $nbSessionThisYear,
                    "shotPerCaliber" => $shotPerCaliber,
        ]);
    }

    /**
     * Display the page updateSession
     * @param type $idSession the id of the session
     * @return type view updateSession
     */
    public function showUpdateSession($idSession) {
        $session = Session::where(['id' => $idSession])->where(['idUser' => Auth::id()])->firstOrFail();
        $gunsOfUser = GunOfUser::where(['idUser' => Auth::id()])->get();
        return view('sessions.updateSession', ['session' => $session, "gunsOfUser" => $gunsOfUser]);
    }

    /**
     * Update the session that the user selected
     * @param Request $request
     * @return back with input and error if information are not good | 
     * on another view updateSession with the modified information and success message
     */
    public function doUpdateSession(Request $request) {
        $validData = Validator::make($request->all(), [
                    'dateSession' => ['required'],
                    'distance' => ['required', 'regex:/[0-9]+/'],
                    'nbTir' => ['required', 'regex:/[0-9]+/'],
                    'caliber' => ['required', 'regex:/([0-9,]+x[0-9]+)|([0-9]+)/'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $session = Session::findOrFail($request->get('idSession'));
        $session->dateSession = $request->get('dateSession');
        $session->idGun = $request->get('weaponUsed');
        $session->cleaned = $request->get('cleaned');
        $session->distance = $request->get('distance');
        $session->nbShoot = $request->get('nbTir');
        $session->caliber = $request->get('caliber');
        $session->updated_at = now();
        try {
            $session->save();
            return redirect()->route('updateSession', ['idSession' => $session->id])->withInput(['successUpdate' => 'La modification de la session a bien eu lieu.']);
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorSave' => "Une erreur est survenue lors de la modification de la session."]);
        }
    }

    /**
     * Display the page deleteSession
     * @param type $idSession the id of the session
     * @return view deleteSession
     */
    public function showDeleteSession($idSession) {
        $session = Session::where(['id' => $idSession])->where(['idUser' => Auth::id()])->firstOrFail();
        return view('sessions.deleteSession', ['session' => $session]);
    }

    /**
     * Delete a session from the database
     * @param Request $request
     * @return back with input and error if it fails |
     * view showSession with success message
     */
    public function doDeleteSession(Request $request) {
        $validData = Validator::make($request->all(), [
                    'supprimer' => ['required', 'in:Supprimer'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $session = Session::find($request->get('idSession'));
        try {
            $session->delete();
            return redirect()->route('showSession')->withInput(['successDelete' => 'Votre session a bien été supprimée.']);
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorDelete' => 'Une erreur est survenue lors de la suppression de la session.']);
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     * @return type
     */
    public function messages() {
        return [
            'distance.regex' => 'La distance doit être un nombre entier.',
            'nbTir.regex' => 'Le nombre de tir doit être un nombre entier.',
            'caliber.regex' => 'Le calibre doit respecter les normes (ex : 22 ; 357 ; 7.5x55',
            'supprimer.in' => 'Il faut écrire : "Supprimer"',
            'required' => 'Le champ :attribute doit être rempli.',
        ];
    }

}
