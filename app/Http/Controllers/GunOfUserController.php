<?php

namespace App\Http\Controllers;

use \App\Models\GunOfUser;
use \App\Models\Gun;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class GunOfUserController extends Controller {

    /**
     * Display the page addGun
     * @param type $category
     * @return type
     */
    public function showAddGunByCategory($category = "Pistol") {
        $categories = DB::table("guns")->select("category")->distinct()->get();
        $guns = Gun::where(["category" => $category])->orderBy("name")->get();
        return view('guns.addGun', ["guns" => $guns, "categories" => $categories]);
    }

    /**
     * Add a gun to a user
     * @param Request $request
     * @return type
     */
    public function doAddGun(Request $request) {
        $category = explode("/", $request->headers->get('referer'));
        $category = $category[count($category) - 1];
        $validData = Validator::make($request->all(), [
                    'dateOfPurchase' => ['required'],
                    'placeOfPurchase' => ['required'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $gunOfUser = new GunOfUser([
            "idUser" => Auth::id(),
            "idGun" => $request->get('weapon'),
            'dateOfPurchase' => $request->get('dateOfPurchase'),
            'placeOfPurchase' => $request->get('placeOfPurchase'),
        ]);
        try {
            $gunOfUser->save();
        } catch (\Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorInsert' => 'Vous possédez déjà cette arme.']);
        }
        return redirect()->route('addGun', ['category' => $category])->withInput(["successInsert" => "Votre arme a bien été ajoutée."]);
    }

    /**
     * Display the page showGunsOfUser
     * @return view showGunsOfUser
     */
    public function showGunsOfUser() {
        $gunsOfUser = GunOfUser::where('idUser', '=', Auth::id())
                ->paginate(10);
        return view("guns.showGunsOfUser", ['gunsOfUser' => $gunsOfUser]);
    }

    /**
     * Display the page sellGun
     * @param Request $request
     * @return view sellGun
     */
    public function sellGun(Request $request) {
        $gunOfUser = GunOfUser::where("idGun", '=', $request->get('idGun'))
                ->where('idUser', '=', Auth::id())
                ->first();
        return view("guns.sellGun", ['gunOfUser' => $gunOfUser]);
    }

    /**
     * Update a gun that the user choose.
     * @param Request $request
     * @return back with input and errors if there is an error | to route showGunsOfUser if it is a success
     */
    public function doSellGun(Request $request) {
        $validData = Validator::make($request->all(), [
                    'dateOfSale' => ['required'],
                    'soldToUser' => ['required'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        try {
            $gunOfUser = DB::update('update gunsOfUser set dateOfSale = ?, soldToUser = ? where idGun = ? and idUser = ?',
                            [
                                $request->get('dateOfSale'),
                                $request->get('soldToUser'),
                                $request->get('idGun'),
                                Auth::id()
            ]);
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorModif' => 'Une erreur est survenue lors de la vente de l\'arme.']);
        }
        return redirect()->route('showGunsOfUser')->withInput(['successUpdate' => 'Votre arme a bien été vendu.']);
    }

    public function detailGun(Request $request){
        $gunOfUser = GunOfUser::where('idGun', '=', $request->get('idGun'))
                ->where('idUser', '=', Auth::id())
                ->first();
        $sessionWithGun = \App\Models\Session::select('nbShoot')
                ->where('idGun', '=', $request->get('idGun'))
                ->where('idUser', '=', Auth::id())
                ->get();
        $nbShoot = $sessionWithGun->sum('nbShoot');
        return view('guns.detailGun', ['gunOfUser' => $gunOfUser, 'nbShoot' => $nbShoot]);
    }
    
    /**
     * Get the error messages for the defined validation rules.
     * @return type
     */
    public function messages() {
        return [
            'placeOfPurchase.regex' => 'L\'adresse doit être conforme.',
            'required' => 'Le champ :attribute doit être rempli.',
        ];
    }

}
